CC=gcc
CFLAGS=-Wall -Isrc/include -DDEBUG -g
LDFLAGS=-lm
SRC=src/points.c src/timer.c src/read_file.c src/distance.c src/brute_force.c src/divide_and_conquer.c
OBJ=$(SRC:.c=.o)

all: points informe

points: $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

informe:
# if pdflatex is installed create the informe
ifneq (, $(shell which pdflatex))
	make -C doc
	mv doc/Informe.pdf Informe.pdf
endif

test:
	make -C test
	test/test

clean: cleanpoints cleaninforme cleantest

cleanpoints:
	rm -f src/*.o points

cleaninforme:
	make -C doc clean
	rm -f Informe.pdf

cleantest:
	make -C test clean

.PHONY: all points informe test clean cleanpoints cleaninforme cleantest
