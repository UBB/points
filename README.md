# Points
Una programa que deja encontar un par de puntos más cercano.
[points](https://git.cromer.cl/UBB/points)

## Autores
Christopher Cromer

Rodolfo Cuevas

## Compilar
Hay tres blancos disponible para usar con GNU Make:

	make points
Compilar el programa

	make test
Comprobar que los algoritmos y programa corren como esperado

	make informe
Compilar el informe si pdflatex está disponible en el sistema

## Correr el programa
	./points
