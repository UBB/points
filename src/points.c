/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include "points.h"
#include "distance.h"
#include "timer.h"
#include "read_file.h"
#include "brute_force.h"
#include "divide_and_conquer.h"

#define POINTS_VERSION "1.0.0"

/**
 * Imprimir el uso del programa
 */
void print_usage() {
	fprintf(stdout, "uso: points [OPCIÓN]\n");
	fprintf(stdout, "  -f, --file                 el archivo de puntos a correr\n");
	fprintf(stdout, "  -a, --all                  usar todos los algoritmos de ordenamentio\n");
	fprintf(stdout, "  -b, --brute                usar brute force\n");
	fprintf(stdout, "  -d, --divide               usar divide and conquer\n");
	fprintf(stdout, "  -h, --help                 mostrar como usar el programa\n");
	fprintf(stdout, "  -v, --version              mostrar la versión del programa\n");
}

/**
 * Empezar los pasos antes de correr
 */
void start_algorithm(const char *message, int n) {
	fprintf(stdout, "%s", message);
	fflush(stdout);
	start_timer();
}

/**
 * Empezar los pasos después de correr
 */
void end_algorithm() {
	stop_timer();
	fprintf(stdout, "done\n");
	print_timer();
	fprintf(stdout, "\n");
}

/**
 * La entrada del programa
 * @param argc La cantidad de argumentos pasado al programa
 * @param argv Los argumento pasado al programa
 * @return Retorna el codigo de error o 0 por exito
 */
int main (int argc, char **argv) {
	double dist = DBL_MAX;
	point_t *points = NULL;
	point_t *closest_points = NULL;
	char *filename = NULL;
	int brute = 0;
	int divide = 0;
	int opt;
	int long_index = 0;
	unsigned int n = 0;
	static struct option long_options[] = {
		{"file",		required_argument,	0,	'f'},
		{"all",			no_argument,		0,	'a'},
		{"brute",		no_argument,		0,	'b'},
		{"divide",		no_argument,		0,	'd'},
		{"help",		no_argument,		0,  'h'},
		{"version",		no_argument,		0,	'v'},
		{0, 0, 0, 0}
	};

	if (argc == 1) {
		print_usage();
		return 0;
	}

	while ((opt = getopt_long(argc, argv, "f:abdhv", long_options, &long_index)) != -1) {
		switch (opt) {
			case 'f':
				if (filename != NULL) {
					free(filename);
					filename = NULL;
				}
				filename = malloc(sizeof(optarg) * strlen(optarg));
				strcpy(filename, optarg);
				break;
			case 'a':
				brute = 1;
				divide = 1;
				break;
			case 'b':
				brute = 1;
				break;
			case 'd':
				divide = 1;
				break;
			case 'h':
				print_usage();
				if (filename != NULL) {
					free(filename);
				}
				return 0;
			case 'v':
				printf("points versión: %s\n", POINTS_VERSION);
				if (filename != NULL) {
					free(filename);
				}
				return 0;
				break;
			default:
				print_usage();
				if (filename != NULL) {
					free(filename);
				}
				return 1;
		}
	}

	if (!brute && !divide) {
		fprintf(stderr, "Error: No se seleccionó un algoritmo valido!\n");
		print_usage();
		if (filename != NULL) {
			free(filename);
		}
		return 2;
	}

	if ((opt = read_file(filename, &points, &n))) {
		if (filename != NULL) {
			free(filename);
		}
		if (opt > 4) {
			// Only print this error if the file was able to be opened
			fprintf(stderr, "Error: El archivo es corrupto!\n");
		}
		return 3;
	}
	if (filename != NULL) {
		free(filename);
	}

	if (n < 2) {
		fprintf(stderr, "Error: Se necesita un minimo de 2 puntos!\n");
		return 4;
	}

	if (brute) {
		start_algorithm("Brute force corriendo... ", n);
		closest_points = brute_force(points, n, &dist);
		end_algorithm();
		fprintf(stdout, "point 1: x: %f y: %f\n", closest_points[0].x, closest_points[0].y);
		fprintf(stdout, "point 2: x: %f y: %f\n", closest_points[1].x, closest_points[1].y);
		fprintf(stdout, "distance: %lf\n\n", sqrt(dist));
		free(closest_points);
		closest_points = NULL;
	}

	if (divide) {
		start_algorithm("Divide and conquer corriendo... ", n);
		closest_points = divide_and_conquer(points, n, &dist);
		end_algorithm();
		fprintf(stdout, "point 1: x: %f y: %f\n", closest_points[0].x, closest_points[0].y);
		fprintf(stdout, "point 2: x: %f y: %f\n", closest_points[1].x, closest_points[1].y);
		fprintf(stdout, "distance: %lf\n", sqrt(dist));
		free(closest_points);
		closest_points = NULL;
	}

	free(points);

	return 0;
}
