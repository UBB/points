/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <sys/stat.h>
#include "points.h"

/**
 * Leer el buffer y guardar el valor si es un float
 * @param variable Donde se guarda el valor del buffer
 * @param buffer El buffer a leer
 * @return Retorna 1 si es exitosa ó 0 si falla
 */
int read_float_buffer(float *variable, char *buffer) {
	char *check;
	errno = 0;
	float input = strtof(buffer, &check);
	if (buffer == check) {
		// Empty
		#ifdef DEBUG
		fprintf(stderr, "Error: Empty!\n");
		#endif
		return 0;
	}
	else if (errno == ERANGE && input == INFINITY) {
		// Overflow
		#ifdef DEBUG
		fprintf(stderr, "Error: Overflow!\n");
		#endif
		return 0;
	}
	else if (errno == ERANGE && input == -INFINITY) {
		// Underflow
		#ifdef DEBUG
		fprintf(stderr, "Error: Underflow!\n");
		#endif
		return 0;
	}
	else if (errno == EINVAL) {  /* not in all c99 implementations - gcc OK */
		// Base contains unsupported value
		// This check is not in all c99 implementations, but does exist in gcc
		#ifdef DEBUG
		fprintf(stderr, "Error: Unsupported value!\n");
		#endif
		return 0;
	}
	else if (errno != 0 && input == 0) {
		// Unspecified error
		#ifdef DEBUG
		fprintf(stderr, "Error: Non specified error!\n");
		#endif
		return 0;
	}
	else if (errno == 0 && !*check) {
		// Valid number
		*variable = input;
		return 1;
	}
	else if (errno == 0 && *check != 0) {
		// Contains non numeric characters
		#ifdef DEBUG
		fprintf(stderr, "Error: Non numeric characters!\n");
		#endif
		return 0;
	}
	else {
		#ifdef DEBUG
		fprintf(stderr, "Error: Unknown error!\n");
		#endif
		return 0;
	}
}

/**
 * Leer el buffer y guardar el valor si es un unsigned int
 * @param variable Donde se guarda el valor del buffer
 * @param buffer El buffer a leer
 * @return Retorna 1 si es exitosa ó 0 si falla
 */
int read_int_buffer(unsigned int *variable, char *buffer) {
	char *check;
	errno = 0;
	long input = strtol(buffer, &check, 10);
	if (buffer == check) {
		// Empty
		#ifdef DEBUG
		fprintf(stderr, "Error: Empty!\n");
		#endif
		return 0;
	}
	else if (errno == ERANGE && input == LONG_MIN) {
		// Overflow
		#ifdef DEBUG
		fprintf(stderr, "Error: Overflow!\n");
		#endif
		return 0;
	}
	else if (errno == ERANGE && input == LONG_MAX) {
		// Underflow
		#ifdef DEBUG
		fprintf(stderr, "Error: Underflow!\n");
		#endif
		return 0;
	}
	else if (errno == EINVAL) {  /* not in all c99 implementations - gcc OK */
		// Base contains unsupported value
		// This check is not in all c99 implementations, but does exist in gcc
		#ifdef DEBUG
		fprintf(stderr, "Error: Unsupported value!\n");
		#endif
		return 0;
	}
	else if (errno != 0 && input == 0) {
		// Unspecified error
		#ifdef DEBUG
		fprintf(stderr, "Error: Non specified error!\n");
		#endif
		return 0;
	}
	else if (errno == 0 && !*check) {
		// Valid number
		*variable = (unsigned int) input;
		return 1;
	}
	else if (errno == 0 && *check != 0) {
		// Contains non numeric characters
		#ifdef DEBUG
		fprintf(stderr, "Error: Non numeric characters!\n");
		#endif
		return 0;
	}
	else {
		#ifdef DEBUG
		fprintf(stderr, "Error: Unknown error!\n");
		#endif
		return 0;
	}
}

/**
 * Leer el archivo de puntos
 * @param filename El nombre del archivo
 * @param points Un arreglo de puntos
 * @param n La cantidad de puntos en el archivo
 * @return Retorna 0 con exito o un numero de error al contrario
 */
int read_file(char *filename, point_t **points, unsigned int *n) {
	FILE *file = NULL;
	ssize_t chars = 0;
	char *buffer = NULL;
	char *token;
	size_t size = 0;
	float coord;
	int i;
	int j = 0;
	struct stat statbuff;

	lstat(filename, &statbuff);
	if (access(filename, F_OK) || !S_ISREG(statbuff.st_mode)) {
		fprintf(stderr, "Error: El archivo \"%s\" no existe!\n", filename);
		return 1;
	}
	if (access(filename, R_OK)) {
		fprintf(stderr, "Error: No se puede leer el archivo \"%s\"!\n", filename);
		return 2;
	}

	file = fopen(filename, "r");
	if (file == NULL) {
		perror("fopen");
		return 4;
	}

	while ((chars = getline(&buffer, &size, file)) != -1) {
		if (*n == 0) {
			for (i = 0; i < chars; i++) {
				if (buffer[i] == ' ' || buffer[i] == '\n' || buffer[i] == '\r' || buffer[i] == '\t') {
					buffer[i] = '\0';
				}
			}
			if (!read_int_buffer(n, buffer)) {
				fprintf(stderr, "Error: Numero de puntos invalido!\n");
				return 5;
			}
			if (*n > UINT_MAX || *n < 1) {
				if (sizeof(unsigned int) == 4) {
					fprintf(stderr, "Error: La cantidad de puntos tiene que ser menor de 4,294,967,295 y mayor de 0\n");
				}
				else {
					fprintf(stderr, "Error: La cantidad de puntos tiene que ser menor de 65,535 y mayor de 0\n");
				}
				return 6;
			}
			*points = realloc(*points, sizeof(point_t) * *n);
		}
		else {
			for (i = 0; i < chars; i++) {
				if (buffer[i] == '\n' || buffer[i] == '\r' || buffer[i] == '\t') {
					buffer[i] = '\0';
				}
			}
			i = 0;
			token = strtok(buffer, " ");
			while (token != NULL) {
				if (i == 0) {
					if (!read_float_buffer(&coord, token)) {
						fprintf(stderr, "Error: Coordinado invalido!\n");
						return 7;
					}
					(*points)[j].x = coord;
				}
				else if (i == 1) {
					if (!read_float_buffer(&coord, token)) {
						fprintf(stderr, "Error: Coordinado invalido!\n");
						return 7;
					}
					(*points)[j].y = coord;
					j++;
				}
				else {
					break;
				}
				i++;
				if (j > *n) {
					fprintf(stderr, "Error: Hay demasiado puntos!\n");
					return 8;
				}
				token = strtok(NULL, " ");
			}
		}
		free(buffer);
		buffer = NULL;
		size = 0;
	}
	if (j < *n || *n < 2) {
		fprintf(stderr, "Error: No hay suficiente puntos!\n");
		return 9;
	}

	return 0;
}
