/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <time.h>
#include <math.h>

/**
 * Cuando empezó el timer
 */
static struct timespec start_time;
/**
 * Cuando terminó el timer
 */
static struct timespec stop_time;

/**
 * Empezar el timer
 */
void start_timer() {
	clock_gettime(CLOCK_MONOTONIC, &start_time);
	stop_time.tv_nsec = 0;
}

/**
 * Terminar el timer
 */
void stop_timer() {
	if (start_time.tv_nsec == 0) {
		fprintf(stderr, "Error: El temporizador no ha comenzado todavía!\n");
	}
	else {
		clock_gettime(CLOCK_MONOTONIC, &stop_time);
	}
}

/**
 * Imprimir el tiempo de ejecución
 */
void print_timer() {
	if (start_time.tv_nsec == 0) {
		fprintf(stderr, "Error: El temporizador no ha comenzado todavía!\n");
	}
	else if (stop_time.tv_nsec == 0) {
		fprintf(stderr, "Error: El temporizador no ha terminado todavía!\n");
	}
	else {
		time_t seconds  = stop_time.tv_sec - start_time.tv_sec;
		unsigned long long milliseconds = round((stop_time.tv_nsec - start_time.tv_nsec)/ 1.0e6); // Convert nanoseconds to milliseconds
		if (milliseconds > 999) {
			seconds++;
			milliseconds = 0;
		}
		unsigned int minutes = 0;
		unsigned int hours = 0;
		if (seconds >= 3600) {
			hours = seconds / 3600;
			seconds = seconds - (hours * 3600);
			if (seconds >= 60) {
				minutes = seconds / 60;
				seconds = seconds - (minutes * 60);
			}
			fprintf(stdout, "Tiempo de ejecución: %d horas, %d minutos y %ld.%lld segundos\n", hours, minutes, seconds, milliseconds);
		}
		else if (seconds >= 60) {
			minutes = seconds / 60;
			seconds = seconds - (minutes * 60);
			fprintf(stdout, "Tiempo de ejecución: %d minutos y %ld.%lld segundos\n", minutes, seconds, milliseconds);
		}
		else {
			fprintf(stdout, "Tiempo de ejecución: %ld.%lld segundos\n", seconds, milliseconds);
		}
	}
}
