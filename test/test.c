/*
 * Copyright 2018 Christopher Cromer
 * Copyright 2018 Rodolfo Cuevas
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <float.h>
#include "points.h"
#include "read_file.h"
#include "brute_force.h"
#include "divide_and_conquer.h"

/**
 * Comparar 2 double para ver si son casi igual
 * @param d1 El primer double
 * @param d2 El segudno double
 * @return Retorna 1 si son igual o 0 sino
 */
inline int compare_double(double d1, double d2) {
	double precision = 0.000000001;
	if (((d1 - precision) < d2) && ((d1 + precision) > d2)) {
		return 1;
	}
	else {
		return 0;
	}
}

/**
 * El programa de test
 * @param argc La cantidad de argumentos
 * @param argv Los argumentos
 * @return Retorna 0 por exito o 1 si falló algun test
 */
int main(int argc, char **argv) {
	int passed = 0;
	int failed = 0;
	point_t *points = NULL;
	unsigned int n = 0;
	double dist = DBL_MAX;

	fprintf(stdout, "Running tests:\n");

	if (read_file("test/100.pto", &points, &n) == 0) {
		fprintf(stdout, "\tread file: passed\n");
		passed++;
	}
	else {
		fprintf(stdout, "\tread file: failed\n");
		failed++;
	}

	if (failed > 0) {
		fprintf(stdout, "\tbrute force: failed\n");
		failed++;
	}
	else {
		fprintf(stdout, "\tbrute force: ");
		fflush(stdout);
		brute_force(points, n, &dist);
		if (compare_double(sqrt(dist), 0.067687840)) {
			fprintf(stdout, "passed\n");
			passed++;
		}
		else {
			fprintf(stdout, "failed\n");
			failed++;
		}
	}

	if (failed > 0) {
		fprintf(stdout, "\tdivide and conquer: failed\n");
		failed++;
	}
	else {
		fprintf(stdout, "\tdivide and conquer: ");
		fflush(stdout);
		divide_and_conquer(points, n, &dist);
		if (compare_double(sqrt(dist), 0.067687840)) {
			fprintf(stdout, "passed\n");
			passed++;
		}
		else {
			fprintf(stdout, "failed\n");
			failed++;
		}
	}

	fprintf(stdout, "%d tests passed\n", passed);
	fprintf(stdout, "%d tests failed\n", failed);

	free(points);

	if (failed == 0) {
		return 0;
	}
	else {
		return 1;
	}
}
